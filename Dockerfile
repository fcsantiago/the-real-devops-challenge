FROM python:3.6-slim
COPY . /code
WORKDIR /code
RUN apt-get update -y && \
apt-get install -y curl jq && \
rm -rf /var/lib/apt/lists/* && \
pip install --no-cache-dir -r requirements.txt
CMD python app.py
