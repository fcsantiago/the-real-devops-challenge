# Information on to how to use MongoDB

## Basic commands:

* Create user

  * Move to bds use
```sh
use name_dbs
```
  * Create normal user:
  ```sh
  db.createUser({
   user:"restaurant",
   pwd:"restaurant123",
   roles:[
    {
       role:"readWrite",
       db:"restaurant"
    }
   ],
   mechanisms:[
    "SCRAM-SHA-1"
   ]
  })
```
  * Create *admin* user:
  ```sh
    db.createUser(
  ... {
  ... user: "restaurant",
  ... pwd: "1234",
  ... roles: ["dbOwner"],
   mechanisms:[
    "SCRAM-SHA-1"
   ]
  ... }
  ... )
  ```
  
  * Create readWrite user
  
    ```sh
    db.createUser(
    {user: "restaurant",
    pwd: "test12",
    roles: [ { role: "readWrite",
    db: "restaurant" }]
    }
    )```
  
> ref: https://professor-falken.com/linux/como-crear-o-eliminar-usuarios-en-mongodb/

*  See DataBase
  
```sh
show dbs
```
*  see collections;
```sh
show collections;
```
* Example of seeing what a collection
```sh
db.restaurant.find();
```
* See user
```sh
 db.getUser('restaurant')
```
> Ref:  https://www.idqweb.com/mongodb-comandos-basicos/

## Intermediate  Commands:
* import data from json

```sh
mongoimport --host localhost -u parzibyte -p hunter2 --port 27017 --db test --collection restaurant --file restaurant.json   --authenticationDatabase "admin"
```
```sh
mongoimport --host 127.0.0.1 -u root -p 1234 --port 27017 --db restaurant --collection restaurant --file restaurant.json   --authenticationDatabase "admin"

```
> *Commands*
```sh
mongoimport --host 127.0.0.1 -u restaurant -p restaurant123 --port 27017 --db restaurant --collection restaurant --file restaurant.json   --authenticationDatabase "admin"
```

> Ref: https://www.idqweb.com/mongodb-comandos-basicos/

> To connect to the database we must export the connection variables
```sh
export MONGO_URI=mongodb://root:1234@mongo:27017/restaurant
```
- - - -
#  Install MongoDB with Helm
## Prerequisites
* Install [Helm](https://helm.sh/docs/intro/install/)
* Install [kubectl](https://kubernetes.io/es/docs/tasks/tools/install-kubectl/#instalar-kubectl)
* Install client for Mongodb

* Add repository
```sh
helm repo add bitnami https://charts.bitnami.com/bitnami
```
* Install

```sh
helm install my-release --set-string mongodbRootPassword=1234,mongodbUsername=admin,mongodbPassword=1234,mongodbDatabase=restaurant bitnami/mongodb
```
* Port Foward
```
kubectl port-forward --namespace default svc/my-release-mongodb 27017:27017 &
```
* Connect to MongoDB
```sh
mongo --host 127.0.0.1 --authenticationDatabase admin -p $MONGODB_ROOT_PASSWORD
```

> Ref: https://github.com/bitnami/charts/tree/master/bitnami/mongodb
